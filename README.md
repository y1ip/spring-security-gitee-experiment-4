
# <p align = "center"> **东莞理工学院网络空间安全学院**  </p>
####  **课程名称** ：企业级开发框架专题</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**学期：2020秋季**   
  

`实验名称`：实验四 基于Spring Security码云OAuth2认证&emsp;&nbsp;&nbsp;&nbsp;`实验序号`：四<br>
`姓名`：叶薰馥 &nbsp;&nbsp;&nbsp;&nbsp;`学号`：201841413237&nbsp;&nbsp;&nbsp;&nbsp;`班级`：18网工2班 <br> 
`实验地址`：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`实验日期`：2020-11-30&nbsp;&nbsp;&nbsp;&nbsp;`同组同学`：无 <br>
`指导老师`：黎志雄 &nbsp;&nbsp; `教师评语`：&nbsp;&nbsp;&nbsp;&nbsp;`实验成绩`：&nbsp;&nbsp;&nbsp;&nbsp;

# 实验四 基于Spring Security码云OAuth2认证


#### 一、实验目的

1、掌握使用Spring Security框架；<br>
2、掌握使用Spring Security的安全过滤链；<br>
3、掌握编写Spring Security单元测试；<br>
4、掌握创建接入码云的应用；<br>
5、掌握码云OAuth2认证基本流程；<br>
6、掌握使用码云API；<br>
7、了解使用模版引擎或前端框架制作用户登录界面<br>


#### 二、实验环境

1、JDK 1.8或更高版本 <br>
2、Maven 3.6+ <br>
3、IntelliJ IDEA <br>

 

#### 三、实验任务
1、登录码云，fork实验四的作业仓库。
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/201959_ef1c71f7_8167141.jpeg"/>
</div>
<br>

 2、根据下面的步骤填充代码，运行并测试成功：
1）OAuth2基本认证流程。
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/204853_24c37277_8167141.jpeg"/>
</div>

```java
public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            // 用户认证前，构造一个GiteeOAuth2LoginAuthenticationToken
            GiteeOAuth2LoginAuthenticationToken authenticationToken = (GiteeOAuth2LoginAuthenticationToken) authentication;
            // 通过码云API获取access_token
            String accessToken = getAccessToken(authenticationToken.getCode());
            // 通过码云API获取Gitee授权用户的资料
            Map<String, Object> userInfo = getUserInfo(accessToken);
            // 认证成功后，重新生成Authentication
            return createSuccessAuthentication(Objects.requireNonNull(userInfo), authenticationToken.getRequest());
        }
```

<p align=center >码云OAuth2认证流程</p>
<br>

2）步骤一：创建接入码云的应用。
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/211534_7016375a_8167141.jpeg"/>
</div>

```java
///////////////////////////////////////////////////
    /// 步骤一：创建接入码云的应用,并把 CLIENT_ID 与 CLIENT_SECRET 赋值给下面的静态成员变量。
    /// 参考：https://gitee.com/api/v5/oauth_doc#/list-item-3
    static final String CLIENT_ID = "f224ce5b4b11c76cb620cb84c2fe18c4375b6c31fd09ad3dc523b4308e7d9662";
    static final String CLIENT_SECRET = "a56b678f8271259195522232f0fe9416df23ff00503f65a9005b0b89889fe825";
    ///////////////////////////////////////////////////
```
<br>

3）步骤二：编写重定向过滤器的业务逻辑。
```java
// 应用通过 浏览器 或 Webview 将用户引导到码云三方认证页面上（ GET请求 ）
            // 重定向地址：https://gitee.com/oauth/authorize?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code
            //////////////////////////////////////////////////////////////
            /// 步骤二：编写重定向过滤器的业务逻辑。
            /// 当用户访问/oauth2/gitee时，本重定向过滤器拦截请求，并将用户重定向到码云三方认证页面上。
            String uriString = UriComponentsBuilder.fromUriString("https://gitee.com/oauth/authorize?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code")
                    .encode()
                    .buildAndExpand(CLIENT_ID, REDIRECT_URI).toUriString();
            response.sendRedirect(uriString);
            //////////////////////////////////////////////////////////////
````
<p align=center >当用户访问/oauth2/gitee时，本重定向过滤器拦截请求，并将用户重定向到码云三方认证页面上</p>
<br>

4）步骤三：使用码云access_token API向码云认证服务器发送post请求获取。

```java
 ////////////////////////////////////////////////////
            /// 步骤三：使用码云access_token API向码云认证服务器发送post请求获取access_token。
            URI uri = UriComponentsBuilder.fromUriString("https://gitee.com/oauth/token?grant_type=authorization_code&code={code}&client_id={client_id}&redirect_uri={redirect_uri}&client_secret={client_secret}")
                    .encode()
                    .buildAndExpand(code, CLIENT_ID, REDIRECT_URI, CLIENT_SECRET).toUri();

            RestTemplate rest= new RestTemplate();

            RequestEntity<Void> requestEntity = RequestEntity.post(uri).header("User-Agent", "Dgut Demo").build();

            ResponseEntity<String> exchange = rest.exchange(requestEntity, String.class);

            String result = exchange.getBody();

            Map<String, Object> resultMap = new JacksonJsonParser().parseMap(result);

            String access_tokenString = (String) resultMap.get("access_token");

            return access_tokenString;
            ////////////////////////////////////////////////////
```
<br>

5）步骤四：使用码云API获取授权用户的资料。

```java
 ////////////////////////////////////////////////////
            /// 步骤四：使用码云API获取授权用户的资料。
            /// 参考：https://gitee.com/api/v5/swagger#/getV5User
            URI uri = UriComponentsBuilder.fromUriString("https://gitee.com/api/v5/user?access_token={access_token}")
                    .encode()
                    .buildAndExpand(accessToken).toUri();

            RestTemplate rest= new RestTemplate();

            RequestEntity<Void> requestEntity = RequestEntity.get(uri).header("User-Agent", "Dgut Demo").build();

            ResponseEntity<String> exchange = rest.exchange(requestEntity, String.class);

            String result = exchange.getBody();

            Map<String, Object> resultMap = new JacksonJsonParser().parseMap(result);

            return resultMap;
            ////////////////////////////////////////////////////
```
<br>

6）步骤五：把自定义的两个Filter加进安全过滤链。

```java
 ////////////////////////////////////////////////////////////////
        /// 步骤五：把自定义的两个Filter加进安全过滤链
        /// 注意：不要加在SecurityContextPersistenceFilter前面就行。
        /// 参考：Spring Security内置的过滤器(注意它们的顺序，很重要)https://docs.spring.io/spring-security/site/docs/current/reference/html5/#servlet-security-filters

        http.addFilterAfter(new GiteeOAuth2RedirectFilter(), LogoutFilter.class);
        http.addFilterAfter(new GiteeOAuth2LoginAuthenticationFilter(),LogoutFilter.class);
        ////////////////////////////////////////////////////////////////
```
<br>

7）步骤六：把自定义的SecurityConfigurer应用到安全过滤链

```java
 ////////////////////////////////////////////////
                    /// 步骤六：把我们自定义的SecurityConfigurer应用到安全过滤链
            http.apply(new GiteeOAuth2LoginConfigurer<>());
////////////////////////////////////////////////
```
<br>

8)步骤七：改造/user接口，返回码云资料给前端；改造user.ftlh模版用于显示用户资料。

```java
////////////////////////////////////
            /// 步骤七：改造/user接口，返回码云用户资料给前端；改造user.ftlh模板用于显示用户资料。
            
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            Object principal = authentication.getPrincipal();

            URI uri = UriComponentsBuilder.fromUriString("https://gitee.com/api/v5/users/" + ((GiteeOAuth2LoginConfigurer.GiteeUserDetail)principal).getUserlogin())
                    .build(42);

            RestTemplate rest= new RestTemplate();

            RequestEntity<Void> requestEntity = RequestEntity.get(uri).header("User-Agent", "Dgut Demo").build();

            ResponseEntity<String> exchange = rest.exchange(requestEntity, String.class);

            String result = exchange.getBody();

            Map<String, Object> resultMap = new JacksonJsonParser().parseMap(result);

            model.addAttribute("userInfo", resultMap);


            return "user";
////////////////////////////////////
```
<p align=center >user.ftlh模板</p>

```
<html>
<style type="text/css">
<head>
    <div id="content">

        </head>
<body>
<form action="/user/logout" method="post">
    <div id="box">
        <div class="title">这是前台</div>
        <title>这是前台</title>
    <#-- 加入CSRF令牌，Spring Security提供。 -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <br><br><span>姓名：${userInfo["name"]}</span><br><br>
    <span>个人主页：${userInfo["html_url"]}</span><br><br>
    <span>用户类型：${userInfo["type"]}</span><br><br>
    <span>邮箱：${userInfo["email"]}</span><br><br>
    <span>个人简介：${userInfo["bio"]}</span><br><br>
    <input class="submit" type="submit" value="退出登录">
    </div>
</form>
</body>
</html>
```
<br>

8）编写单元测试。模拟一个登陆用户，访问受保护的接口/test，断言接口的返回内容body部分是否一致。

```java
   @Test
    @WithMockUser(authorities = {"USER"})
    public void test() throws Exception {
        ////////////////////////////////////////////
        /// 步骤八：模拟一个登录用户，访问受保护的接口/test，断言接口的返回内容body部分是否一致。
        ////////////////////////////////////////////
        mvc.perform(MockMvcRequestBuilders.get("/test"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("访问/test接口成功,你拥有USER权限。"));
    }
```
<br>


#### 四、实验结果
>登录页面
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/214433_c6be5466_8167141.jpeg"/>
</div>

>点击第三方账号gitee登录，跳转至gitee登录页面
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/214627_b3576b50_8167141.jpeg"/>
</div>

>登录成功后的个人信息页面
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/214723_f86fce47_8167141.jpeg"/>
</div>

>成功通过单元测试
<div align=center>
    <img width = '50%' height ='50%' src ="https://images.gitee.com/uploads/images/2020/1202/214958_2c109dcc_8167141.jpeg"/>
</div>



