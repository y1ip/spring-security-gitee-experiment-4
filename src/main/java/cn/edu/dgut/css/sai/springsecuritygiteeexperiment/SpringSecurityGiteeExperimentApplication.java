package cn.edu.dgut.css.sai.springsecuritygiteeexperiment;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.security.web.method.annotation.CurrentSecurityContextArgumentResolver;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestMethodArgumentResolver;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.Map;

@SpringBootApplication
public class SpringSecurityGiteeExperimentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityGiteeExperimentApplication.class, args);
    }

    @SuppressWarnings("JavadocReference")
    @Configuration
    static class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

        /**
         * 重写{@link WebSecurityConfigurerAdapter#configure(AuthenticationManagerBuilder)}方法，会实例化一个这条过滤链本地专用的{@link AuthenticationManager}，
         * 这时不会使用全局的{@link AuthenticationConfiguration#getAuthenticationManager()}。
         * <p>
         * 可以定义多个{@link WebSecurityConfigurerAdapter}的子类，每个子类会被Spring Security框架解释为一条安全过滤链{@link SecurityFilterChain},它们由{@link FilterChainProxy}匹配、选择使用。
         * <p>
         * <p>
         * Spring Security的自动配置类{@link AuthenticationConfiguration}声明了一个{@link InitializeUserDetailsBeanManagerConfigurer}Bean，下面说明它有什么作用：
         * {@link InitializeUserDetailsBeanManagerConfigurer} 继承了{@link GlobalAuthenticationConfigurerAdapter} 会用于配置全局AuthenticationManager{@link AuthenticationConfiguration#getAuthenticationManager()}。
         * {@link InitializeUserDetailsBeanManagerConfigurer.InitializeUserDetailsManagerConfigurer#configure(AuthenticationManagerBuilder)}方法会检查是否有{@link UserDetailsService}的Bean在容器中，
         * 如果有的话，会实例化一个{@link DaoAuthenticationProvider},并添加到全局AuthenticationManager中;
         * 它还会检查是否有{@link PasswordEncoder}的Bean在容器，如果有的话，会配置在{@link DaoAuthenticationProvider}中。
         * 总结：{@link InitializeUserDetailsBeanManagerConfigurer}的作用就是帮助全局{@link AuthenticationManager}初始化一个{@link DaoAuthenticationProvider}
         *
         * @see SecurityAutoConfiguration
         * @see EnableGlobalAuthentication
         * @see AuthenticationConfiguration
         * @see WebSecurityConfigurerAdapter#getHttp()
         * @see WebSecurityConfigurerAdapter#authenticationManager()
         * @see AbstractDaoAuthenticationConfigurer
         * @see InMemoryUserDetailsManagerConfigurer
         * @see InMemoryUserDetailsManager
         * @see DaoAuthenticationProvider
         */
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles("roleB").authorities("ADMIN");
        }

        @Bean
        UserDetailsService CustomUserDetailsService() {
            return new InMemoryUserDetailsManager(User.withUsername("sai").password("{noop}sai").roles("USER").authorities("doIt").build());
        }

        /**
         * 设置忽略静态资源的身份认证
         */
        @Override
        public void configure(WebSecurity web) {
            web.ignoring().antMatchers("/css/**", "/img/**", "/**/*.png");
        }

        /**
         * 配置自定义的登录界面，并设置允许所有人访问(必须)。
         * 注意：post请求是默认有CSRF保护。请在自定义的登录界面的表单中加入CSRF令牌一并提交。或配置.csrf().disable()；
         *
         * @see WebSecurityConfigurerAdapter#getHttp()
         * @see UsernamePasswordAuthenticationFilter
         * @see HttpSecurity#formLogin()
         * @see FormLoginConfigurer
         * @see AbstractAuthenticationFilterConfigurer#init(HttpSecurityBuilder)
         * @see AbstractAuthenticationFilterConfigurer#updateAuthenticationDefaults()
         * @see AbstractAuthenticationFilterConfigurer#updateAccessDefaults(HttpSecurityBuilder)
         * @see AbstractAuthenticationFilterConfigurer#configure(HttpSecurityBuilder)
         * @see HttpSecurity#logout()
         * @see LogoutConfigurer
         * @see LogoutConfigurer#getLogoutRequestMatcher(HttpSecurityBuilder) 如果CSRF关闭，则对退出登录的请求方法不限制；如果CSRF启用(默认)，只支持post请求。
         */
        @Override
        protected void configure(HttpSecurity http) throws Exception {

            // @formatter:off
            http
                    // 配置安全过滤链只拦载检查 '/admin/**' 的请求，并配置需要的权限。
                    .antMatcher("/admin/**")
                        .authorizeRequests().anyRequest().hasAnyAuthority("ADMIN").and()
                    // 自定义登录界面
                    .formLogin()
                        .loginPage("/admin/login_backend").permitAll()
                        // 设置默认身份认证成功后跳转的页面,即直接访问登录界面时，认证成功后跳转的页面。
                        .defaultSuccessUrl("/admin").and()
                    // 自定义退出登录请求Url、成功退出登录后的重定向Url
                    .logout()
                        .logoutUrl("/admin/logout").permitAll()
                        .logoutSuccessUrl("/admin/login_backend?logout");
            // @formatter:on
        }
    }

    /**
     * 另一条安全过滤链，匹配所有请求。
     * {@link WebSecurityConfigurerAdapter}默认order是100,不能有两个相同的order值WebSecurityConfigurerAdapter。order值越小，越优先匹配。
     *
     * @see FilterChainProxy#getFilters(HttpServletRequest) for循环遍历。
     * @see Order
     * @see WebSecurityConfigurerAdapter
     */
    @SuppressWarnings("JavadocReference")
    @Configuration
    @Order(101)
    static class UserSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            // @formatter:off
            auth.inMemoryAuthentication()
                    // 新增一个 role ，框架内部处理时相当于加了 "ROLE_" 前缀 的 authority , 详细看源码。
                    .withUser("user").password("{noop}user").roles("roleA").authorities("USER")
                        .and()
                    .withUser("tony").password("{noop}tony").authorities("TONY");
            // @formatter:on
        }

        /**
         * 前台安全过滤链配置。
         * <p></p>
         * 这里我们关注另一个问题，异常问题。
         * <p>
         * Spring Security主要分两种：{@link AuthenticationException}认证异常、{@link AccessDeniedException}访问拒绝异常，它们由{@link ExceptionTranslationFilter}捕获处理。<br/>具体逻辑看{@link ExceptionTranslationFilter#handleSpringSecurityException(HttpServletRequest, HttpServletResponse, FilterChain, RuntimeException)}
         * <p></p>
         * 由上面的{@code handleSpringSecurityException}方法源码可知：<br>
         * 认证异常被捕获后交由{@link AuthenticationEntryPoint}处理；<p>
         * 访问拒绝异常被捕获后由{@link AccessDeniedHandler}处理。<p>
         * 注意的是，如果用户没有认证，即是匿名用户，抛出{@link AccessDeniedException}时会交由{@link AuthenticationEntryPoint}处理。
         * <p></p>
         * <p>
         * {@link WebSecurityConfigurerAdapter#getHttp()}查看这个方法的源码，了解默认调用了{@link HttpSecurity#exceptionHandling()}方法；<br>
         * {@link HttpSecurity#exceptionHandling()}方法会添加{@link ExceptionHandlingConfigurer}配置器；<br>
         * {@link ExceptionHandlingConfigurer} 配置{@link AuthenticationEntryPoint}和{@link AccessDeniedHandler}，把它们赋值给{@link ExceptionTranslationFilter}，最后把{@link ExceptionTranslationFilter}加入安全过滤链。<p></p>
         *
         * @see FilterComparator
         * @see LoginUrlAuthenticationEntryPoint
         * @see AccessDeniedHandlerImpl
         */
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // @formatter:off
            // 不指定path,本安全过滤链会匹配所有请求。
            http
                    .authorizeRequests()
                        .antMatchers("/").permitAll()// 首页放行
                        .anyRequest().hasAnyAuthority("USER").and()
                    .formLogin()
                        .loginPage("/user/login_frontend").permitAll()
                        .defaultSuccessUrl("/user").and()
                    .logout()
                        .logoutUrl("/user/logout").permitAll()
                        .logoutSuccessUrl("/user/login_frontend?logout").and()
                    // 自定义访问拒绝异常处理逻辑
                    // 使用 tony/tony 账号测试权限不符合的返回信息。
                    .exceptionHandling()
                        .accessDeniedHandler(UserSecurityConfig::accessDeniedHandle)
                    ////////////////////////////////////////////////
                    /// 步骤六：把我们自定义的SecurityConfigurer应用到安全过滤链
                    ////////////////////////////////////////////////
            ;

            http.apply(new GiteeOAuth2LoginConfigurer<>());
            // @formatter:on
        }

        /**
         * @see AccessDeniedHandlerImpl#handle(HttpServletRequest, HttpServletResponse, AccessDeniedException)
         */
        private static void accessDeniedHandle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
            request.setAttribute(WebAttributes.ACCESS_DENIED_403,
                    accessDeniedException);
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.setCharacterEncoding(Charset.defaultCharset().displayName());// 解决中文乱码
            response.addHeader("Content-Type", MediaType.TEXT_HTML_VALUE);
            response.getWriter().write("你的权限不够");
        }
    }

    @Controller
    static class UserLoginController {

        // 前台-登录界面
        @GetMapping("/user/login_frontend")
        String userLogin() {
            return "login_frontend";
        }

        /**
         * 获取登录用户的资料接口
         *
         * @see Authentication
         * @see Principal
         * @see Model
         */
        @GetMapping("/user")
        String userIndex(Authentication auth,Model model) {
            ////////////////////////////////////
            /// 步骤七：改造/user接口，返回码云用户资料给前端；改造user.ftlh模板用于显示用户资料。
            ////////////////////////////////////
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            Object principal = authentication.getPrincipal();

            URI uri = UriComponentsBuilder.fromUriString("https://gitee.com/api/v5/users/" + ((GiteeOAuth2LoginConfigurer.GiteeUserDetail)principal).getUserlogin())
                    .build(42);

            RestTemplate rest= new RestTemplate();

            RequestEntity<Void> requestEntity = RequestEntity.get(uri).header("User-Agent", "Dgut Demo").build();

            ResponseEntity<String> exchange = rest.exchange(requestEntity, String.class);

            String result = exchange.getBody();

            Map<String, Object> resultMap = new JacksonJsonParser().parseMap(result);

            model.addAttribute("userInfo", resultMap);



            /**if (principal instanceof UserDetails) {
                String username = ((UserDetails)principal).getUsername();
                model.addAttribute("userName", username);
            } else {
                String username = principal.toString();
            }*/






            return "user";

        }

        /**
         * <p><b>基于 Spring Security 的 spring mvc 参数解释器详细说明:</b></p>
         * <li>
         * 在初始化过滤链时默认执行了 http.servletApi() ，会注册{@link SecurityContextHolderAwareRequestFilter}，
         * 这个过滤器会使用内部类{@code Servlet3SecurityContextHolderAwareRequestWrapper}包装{@link HttpServletRequest},
         * 从而可以实现在 Servlet 3.x 中{@link HttpServletRequest}中新的方法，如：{@link HttpServletRequest#getUserPrincipal()}。
         * spring mvc的其中一个参数解释器{@link ServletRequestMethodArgumentResolver} 可以解释{@link Authentication}类型的参数，原理就是使用{@link HttpServletRequest#getUserPrincipal()}，
         * 注：{@link Authentication}类型继承于{@link java.security.Principal}。
         * </li>
         * <li>
         * {@link AuthenticationPrincipal}注解的参数是由spring mvc参数解释器{@link AuthenticationPrincipalArgumentResolver}解释的，注意：{@link AuthenticationPrincipal#expression()}属性可以设置SpEL表达式，从而获取特定方法的值。
         * </li>
         * <li>{@link CurrentSecurityContext}注解的参数是由spring mvc的参数解释器{@link CurrentSecurityContextArgumentResolver}解释的，注意：{@link CurrentSecurityContext#expression()}属性可以设置SpEL表达式。</li>
         * <li>
         * 以上基于 Spring Security 的 spring mvc 参数解释器是由{@code WebMvcSecurityConfiguration}配置类添加到spring mvc的，它是由{@link EnableWebSecurity}注解中的{@code SpringWebMvcImportSelector}导入的配置类。
         * 如果是Spring Boot应用程序，则这些参数解释器是因为spring boot内部提供的自动配置类{@link SecurityAutoConfiguration}中import的{@code WebSecurityEnablerConfiguration}上有{@link EnableWebSecurity}而注册的。
         * </li>
         *
         * @see SecurityContextHolderAwareRequestFilter
         * @see ServletRequestMethodArgumentResolver
         * @see AuthenticationPrincipalArgumentResolver
         * @see CurrentSecurityContextArgumentResolver
         * @see SecurityContext
         * @see SecurityContextImpl
         */
        @GetMapping("test") // http://localhost:8080/test
        @ResponseBody
        String test(Authentication auth1,
                    @AuthenticationPrincipal UserDetails userDetails,
                    @CurrentSecurityContext SecurityContext securityContext,
                    @CurrentSecurityContext(expression = "authentication") Authentication auth2) {
            System.out.println("auth1 = " + auth1);
            System.out.println("userDetails = " + userDetails);
            System.out.println("securityContext = " + securityContext);
            System.out.println("auth2 = " + auth2);
            return "访问/test接口成功,你拥有USER权限。";
        }
    }

    @Controller
    static class AdminLoginController {
        // 后台-登录界面
        @GetMapping("/admin/login_backend")
        public String adminLogin() {
            return "login_backend";
        }

        // 后台-首页
        @GetMapping("/admin")
        public String adminIndex() {
            return "admin";
        }
    }
}
